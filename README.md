Collection of nix templates.

# Usage
```bash
nix flake init --template "gitlab:amaihoefner/nix-templates#<template>"
```
or
```bash
nix flake new --template "gitlab:amaihoefner/nix-templates#<template>" ./my-new-project
```
