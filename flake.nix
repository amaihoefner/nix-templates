{
  description = "A collection of flake templates";

  outputs = {self}: {
    defaultTemplate = self.templates.default;

    templates = {
      default = {
        path = ./default;
        description = "A simple nix flake setup.";
      };
      haskell = {
        path = ./haskell;
        description = "Nix flake template for haskell projects";
        welcomeText = ''
          Initialize the project with `cabal init`.
        '';
      };
      python = {
        path = ./python;
        description = "Python project using poetry2nix.";
        welcomeText = ''
          To get started:
            - rename the project (in the pyproject.toml, the directory and the package in flake.nix)
            - edit the authors in the pyproject.toml
        '';
      };
      rust = {
        path = ./rust;
        description = "Rust project using poetry2nix.";
      };
      leptos = {
        path = ./leptos;
        description = "Rust leptos project.";
      };
      kernel-module = {
        path = ./kernel-module;
        description = "A template for developing linux kernel modules.";
        welcomeText = ''
          Remember to adjust variables in Makefile.
        '';
      };
    };
  };
}
