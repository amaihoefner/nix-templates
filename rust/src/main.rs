//! Docs

#![forbid(
    missing_debug_implementations,
    unused_extern_crates,
    unused_import_braces,
    unused_qualifications
)]
#![deny(trivial_casts, trivial_numeric_casts)]
#![warn(missing_docs)]

fn main() {
    println!("Hello, world!");
}
