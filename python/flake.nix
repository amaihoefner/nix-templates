{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";

    flake-utils.url = "github:numtide/flake-utils";
    pre-commit-hooks.url = "github:cachix/pre-commit-hooks.nix";

    poetry2nix = {
      url = "github:nix-community/poetry2nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = {self, ...} @ inputs:
    inputs.flake-utils.lib.eachDefaultSystem (system: let
      pkgs = import inputs.nixpkgs {
        inherit system;
        config.allowUnfree = true;
      };
      inherit (inputs.poetry2nix.lib.mkPoetry2Nix {inherit pkgs;}) mkPoetryApplication;
    in {
      checks = {
        pre-commit-check = inputs.pre-commit-hooks.lib.${system}.run {
          src = ./.;
          hooks = {
            # Nix
            alejandra.enable = true;
            # Python
            black.enable = true;
            ruff.enable = true;
            isort.enable = true;
            pyright.enable = true;
            poetry-check = {
              enable = true;
              name = "poetry-check";
              description = "run poetry check to validate config";
              entry = "poetry check";
              language = "python";
              pass_filenames = false;
              files = "^(.*/)?pyproject\.toml$";
            };
            poetry-lock = {
              enable = true;
              name = "poetry-lock";
              description = "run poetry lock to update lock file";
              entry = "poetry lock";
              language = "python";
              pass_filenames = false;
              files = "^(.*/)?(poetry\.lock|pyproject\.toml)$";
            };
            poetry-install = {
              enable = true;
              name = "poetry-install";
              description = "run poetry install to install dependencies from the lock file";
              entry = "poetry install";
              language = "python";
              pass_filenames = false;
              stages = ["post-checkout" "post-merge"];
              always_run = true;
            };
          };
        };
      };
      packages = {
        myapp = mkPoetryApplication {
          projectDir = self;
        };
        default = self.packages.${system}.myapp;
      };

      devShells.default = pkgs.mkShell {
        inputsFrom = [self.packages.${system}.myapp];
        packages = with pkgs; [
          poetry
        ];
        shellHook = ''
          ${self.checks.${system}.pre-commit-check.shellHook}
        '';
      };
    });
}
