/* Licensed under AGPLv3 - Christina Sørensen 2023 */
/* https://nixlang.wiki/en/nix/developing_kernel_modules */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/random.h>
#include <linux/timer.h>

#define MODULE_NAME "hello_nix"

MODULE_LICENSE("AGPL v3");

static struct timer_list hello_timer;

unsigned random_ticket;

void hello_timer_callback(struct timer_list *t) {
  printk(KERN_INFO "Hello, NixOS!\n");
  get_random_bytes(&random_ticket, sizeof random_ticket);
  random_ticket = 1u + (random_ticket % 7u);
  hello_timer.expires = jiffies + HZ * random_ticket * 10;
  add_timer(&hello_timer);
}

static int __init hello_init(void) {
  printk(KERN_INFO "hello_nix: Starting the hello_nix module\n");
  timer_setup(&hello_timer, hello_timer_callback, 0);
  hello_timer.expires = jiffies + HZ * 10;
  add_timer(&hello_timer);
  printk(KERN_NOTICE "hello_nix: Initialized the hello_nix module!\n");
  return 0;
}

static void __exit hello_exit(void) {
  del_timer_sync(&hello_timer);
  printk(KERN_INFO "hello_nix: Removing the hello_nix module\n");
}

module_init(hello_init);
module_exit(hello_exit);
