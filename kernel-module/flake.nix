{
  description = "Example of developing kernel modules with nix";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/23.11";
  };

  outputs = {
    self,
    nixpkgs,
  }: let
    system = "x86_64-linux";
    pkgs = import nixpkgs {
      inherit system;
    };
  in {
    devShells.${system}.default = pkgs.mkShell {
      buildInputs = with pkgs; [
        gnumake
        linux.dev
        ccls
      ];
    };
  };
}
